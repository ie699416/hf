clc
clear all

S = sym('S', 2)

S1_1 = phd2cmplx(0.15,-155);
S1_2 = phd2cmplx(0.85,35);

S2_1 = phd2cmplx(0.9,35);
S2_2 = phd2cmplx(0.15,-155);



SMAT = [S1_1 S1_2  ; 
        S2_1 S2_2  ]
    
 SMAT_t = (SMAT)'
 SMAT_c = conj(SMAT)
 SMAT_ct = SMAT_c'
 
 SMAT_t * SMAT_c
 SMAT * SMAT_ct
 I = eye(2);
 Z = (I + SMAT) * inv(I - SMAT)
 Y = inv(Z)
    
 S1_1*conj(S1_1) +  S2_1*conj(S2_1)
 
 
GAMMA50 =cReflect(50,50)
GAMMA75 =cReflect(75,50)

%%
% Ejercicio 6
clear variables
close all
clc

f = 60;
Vs = 110;
Rs = 0.75;
Is = 110 / 0.75;
Zo = 75;
Rl = 55 + 1i*2*pi*60*0.05
Gl = 1/Rl
l =350000;
c = 0.3e9;


Yo = 1/Zo;

beta = 2*pi*f/c

M1 = [1 Rs ; 0 1];
M2 = [0.1 0 ; 0 10]; 
M3 = [cos(beta*l) 1i*Zo*sin(beta*l) ; 1i*Yo*sin(beta*l) cos(beta*l)]
M4 = [1 0 ; Gl 1];

M_abcd = (M1)*(M2)*(M3)*(M4)

invM_abcd = inv(M_abcd)

VL = invM_abcd(1,1)*Vs + invM_abcd(2,1)*Is

VL_PHD = cmplx2phd(VL)

