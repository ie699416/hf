<ADSWorkspace Revision="1" Version="100">
    <Workspace Name="">
        <LibraryDefs Name="lib.defs" />
        <ConfigFile Name="de_sim.cfg" />
        <ConfigFile Name="hpeesofsim.cfg" />
        <Log Name="search_history.log" />
        <Preferences Name="layout.prf" />
        <Preferences Name="schematic.prf" />
        <Library Name="ads_standard_layers" />
        <Library Name="ads_schematic_layers" />
        <Library Name="empro_standard_layers" />
        <Library Name="ads_builtin" />
        <Library Name="ads_standard_layers_ic" />
        <Library Name="ads_schematic_layers_ic" />
        <Library Name="ads_schematic_ports_ic" />
        <Library Name="ads_rflib" />
        <Library Name="ads_sources" />
        <Library Name="ads_simulation" />
        <Library Name="ads_tlines" />
        <Library Name="ads_bondwires" />
        <Library Name="ads_datacmps" />
        <Library Name="ads_behavioral" />
        <Library Name="ads_textfonts" />
        <Library Name="ads_common_cmps" />
        <Library Name="ads_designs" />
        <Library Name="ads_verification_test_bench" />
        <Library Name="ads_pelib" />
        <Library Name="Problems_4_lib" />
        <Cell Name="Problems_4_lib:Ex_1_TL_DC_BD" />
    </Workspace>
</ADSWorkspace>
