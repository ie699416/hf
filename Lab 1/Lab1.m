%%
clc
clear all
close all
mil = 1;

Er = 3.9
H = 60*mil; 
step = mil /1000;
W1 = 124.9587
W2 = 30.2353
WQ = 66.5185;

ee0 = (Er+1)/2 + ((Er - 1)/2).* (1./(sqrt(1 +10.*(H./W1))))
ee1 = (Er+1)/2 + ((Er - 1)/2).* (1./(sqrt(1 +10.*(H./W2))))
eeq = (Er+1)/2 + ((Er - 1)/2).* (1./(sqrt(1 +10.*(H./WQ))))


F = 3e9;
c = 0.3e9;
Vp = c/sqrt(eeq)

lambda = Vp/F 
L_4 = (lambda/4) % cm
L_4mil = L_4 / (1e-3 * 0.0254) %meters / mils



Zo = (120*pi./sqrt(ee0)).* (1./((W1*1/H) + 1.393+0.667*log((W1*1/H)+1.444) ))
ZL = (60/sqrt(ee1))* log(8*(H/W2) + 0.25*(W2/H))
ZQ2 = (120*pi./sqrt(eeq)).* (1./((WQ*1/H) + 1.393+0.667*log((WQ*1/H)+1.444) ))

ZQ = sqrt(Zo * ZL)


%%
%% Inciso C
clear; clc;

Z0_line = 50;
Zin = Z0_line;
ZL = 100;
syms Z0_transf
%Zin = Z0_transf*((ZL+1i*Z0_transf*tan(pi/2))/(Z0_transf+1i*ZL*tan(pi/2)));
eqn = Z0_transf*((ZL+1i*Z0_transf*tan(pi/2))/(Z0_transf+1i*ZL*tan(pi/2)))*1/Zin -1;
Z0_par = abs(double(vpasolve(eqn,Z0_transf)));
Z0 = Z0_par(1)

Er = 3.9;
H  = 60 ;
syms W;
eqn = (60/sqrt((Er+1)/2 + (Er-1)/(2*sqrt(1+(1*H)/W))))*log((8*H)/W + W/(4*H))*(1/Z0) - 1;
W1 = double(vpasolve(eqn,W,H));
eqn = ((120*pi)/sqrt((Er+1)/2 + (Er-1)/(2*sqrt(1+(10*H)/W)))) * 1/((W/H)+1.393+(2/3)*log(W/H +1.444)) * 1/Z0 -1;
W2 = double(vpasolve(eqn,W));
clear W;
if(W1/H <= 1)
    W = W1
end
if(W2/H > 1)
    W = W2
end

f = 3E9;
Ee = (Er+1)/2 + (Er-1)/(2*sqrt(1+(10*H)/W));
vp = 299792458/sqrt(Ee);
lambda = vp/f;
L = lambda/4
L = vpa(L)*vpa(39370.078740157)

