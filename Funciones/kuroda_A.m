function [Z1n2, Z2n2] = kuroda_A(Z1,Z2)
n2 = 1 + Z2/Z1;
Z1n2 = Z1/n2; 
Z2n2 = Z2/n2; 
end