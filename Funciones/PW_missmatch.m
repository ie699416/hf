%% Function: Power for load mismatches (Zl != Zo != Rs) 
function PW = PW_missmatch(Vg, Zin, Zg)
Rin = real(Zin);
Xin = imag(Zin);
Rg = real(Zg);
Xg = imag(Zg);
PW  = (0.5) *(abs(Vg)^2)*(Rin/((Rin+Rg)^2 + (Xin + Xg)^2));
end