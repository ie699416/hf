%% Function: Reflection coefficient
function GAMMA =cReflect(Zl,Zo)
GAMMA = (Zl - Zo)/( Zl + Zo);
end