function [n2Z1, n2Z2] = kuroda_B(Z1,Z2)
n2 = 1 + Z2/Z1;
n2Z1 = n2*Z1; 
n2Z2 = n2*Z2; 
end