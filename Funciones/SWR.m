%% Function : SWR
function swr = SWR (GAMMA)
swr = (1 + abs(GAMMA)) / (1 - abs(GAMMA));
end