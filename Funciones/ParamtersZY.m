%% Parameters Z
% A two-port network has the following impedances: Za, Zb and Zc
% Find the Z matrix
clear
clc

V = sym('V',[2 1])
I = sym('I', [2 1])
syms Z(Za,Zb,Zc) 2

Z1_1(Za,Zb,Zc) = Za + Zc
Z1_2(Za,Zb,Zc) = Zc
Z2_1(Za,Zb,Zc) = Zc
Z2_2(Za,Zb,Zc) = Zb + Zc

A= subs(Z)
Z_mat = A(5 + 3i,7 -7i,30+2i)

%% Parameters Y 
% A two-port network has the following admitances: Ya, Yb and Yc
% Find the Y matrix
% Note: Shunt admitances get sum
clear
clc

V = sym('V',[2 1])
I = sym('I', [2 1])
syms Y(Ya,Yb,Yc) 2

Y1_1(Ya,Yb,Yc) = Ya + Yc
Y1_2(Ya,Yb,Yc) = -Yc
Y2_1(Ya,Yb,Yc) = -Yc
Y2_2(Ya,Yb,Yc) = Yb + Yc

A= subs(Y)
Y_mat = A(5 + 3i,7 -7i,30+2i)




