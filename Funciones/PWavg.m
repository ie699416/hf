%% Function: Average power when matched to the generator (Zo = Rs) 
function PW_avg = PWavg(Vo_plus, GAMMA, Rs)
PW_avg  =(abs(Vo_plus)^2)*(1 - (abs(GAMMA))^2)/(2*Rs);
end