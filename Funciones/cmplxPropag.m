%% Function: Complex propagation constant
function g = cmplxPropag(R, L, G, C, w)
 g = sqrt((R + 1i*w*L)*(G + 1i*w*C));
end