%% Function: Complex characteristic impedance
function Zo= Z0(R, L, G, C, w)
    % When lossless, frequency is irrelevant.
    Zo = sqrt(((R + 1i*w*L)/(G + 1i*w*C)));
end