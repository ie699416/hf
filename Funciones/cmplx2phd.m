%% Function: complex to  phassor in degrees
function phd = cmplx2phd(z)
phd = [abs(z) atan2d(imag(z),real(z))];
end