%% Function: Vo Plus in function of the length of the TL.
% Function may be used with a linear combination of {B, l}
function Vo_plus = Vo_plusL(Vin,B,l,GAMMA)
Vo_plus = (Vin)/ ((exp(1i*B*l) + GAMMA *exp(-1i*B*l )) );
end