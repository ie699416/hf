%% Function: complex to  phassor in degrees
function complex = phd2cmplx(Mag, deg)
X = cos(deg*pi/180);
Y = sin(deg*pi/180);
complex = Mag*(X + 1i* Y) ;     
end