%% Function Lossy Zin Gamma l
function Zin = Gl_Zin(Zo, Zl, g, l)
    Zin = (Zo)*((Zl + Zo*tanh(g*l))/(Zo + Zl*tanh(g*l)));
end