%% Parameters S
% A two-port network has the following Impedances: Za, Zb and Zc
% Find the Z matrix
% Note: Shunt admitances get sum
clear
clc
%                                                                          
% 0------[Za] ------ [Zb] ------0                                                                         
% +              |              +                                             
% V1            [Zc]            V2                                               
% -              |              -                                             
% 0-----------------------------0                                                                             
%                                                                          

Zo = 50;
Vi_plus = sym('Vi_plus',[2 1])
Vi_minus = sym('Vi_minus', [2 1])
syms S(Za,Zb,Zc) 2

%   [Vi_minus1 / Vi_plus1 ]|  Vi_plus2 = 0 
%
%   The refflection in the transmition line for the First port is known as
%   Gamma = Zin - Zo / Zin + Zo = Vi_minus1 / Vi_plus1



S1_1(Za,Zb,Zc) = Z(Za + ZB);

%   [Vi_minus2 / Vi_plus1 ]|  Vi_plus2 = 0 
%
%   V1 = Vi_plus1 + Vi_minus1
%   V2 = Vi_plus2 + Vi_minus2 -> V2 = 0 + Vi_minus2
%


S1_2(Za,Zb,Zc) = Zo;

%   [Vi_minus1 / Vi_plus2 ]|  Vi_plus2 = 0 
S2_1(Za,Zb,Zc) = Zo;

%   [Vi_minus2 / Vi_plus2 ]|  Vi_plus2 = 0 
S2_2(Za,Zb,Zc) = Zo;

A= subs(S)
S_mat = A(5 + 3i,7 -7i,30+2i)



