%% Function: cmplx2ph
function ph = cmplx2ph(z)
ph = [abs(z) atan2d(imag(z),real(z))];
end