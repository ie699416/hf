%% Function: Vo Plus in function of reflection coef.
% Function may be used with a linear combination of {B, l}
function Vo_plus_R = Vo_plusReflect(Vin,B,l,GAMMA_L,GAMMA_G)
Vo_plus_R = Vin*(exp(-1i*B*l))/(1-GAMMA_G*GAMMA_L*exp(-1i*2*B*l));
end