%% Function Lossless Zin Betha l
function Zin = Bl_Zin(Zo, Zl, b, l)
    Zin = (Zo)*((Zl + Zo*1i*tan(b*l))/(Zo + Zl*1i*tan(b*l)));
end