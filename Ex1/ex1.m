%% Global variables definition.
%--------------------------------------------------------------------------
global p n u m Ki Me Gi;

p = 1e-12;
n = 1e-9;
u = 1e-6;
m = 1e-3;
Ki = 1e3;
Me = 1e6;
Gi = 1e9;



%% Ex 1 - 2
%--------------------------------------------------------------------------
%  
%--------------------------------------------------------------------------
close all
clc

Zo = 75;
er = 2.25
Dd  = ((Zo * sqrt(er) ) / 138);
Cap = (7.354 * er ) / Dd        %pF
Ind = 140.4 * Dd                %nH






%% Ex 1 - 3 
%--------------------------------------------------------------------------
%   A lossless transmission line has
%   Vs = 3.3 V
%   Rs = 70
%   Zo = 50 Ohm/m (not matched)
%   Zl =118 + j85
%   L = ? H/m
%   C = ? F/m 
%   R = 0 Ohm/m  
%   G = 0 S/m. 
%
%   Calculate the power delivered to the load if:
%   -   a) l = lambda /2 
%   -   b) l = lambda /4
%
%   Why is the power delivered different?
%--------------------------------------------------------------------------
close all
clc

Rs = 70;
Zo = 50;
Zl = 118 + 85*1i;
Vs = 3.3; 

% Reflection coef. at the load. 
GAMMA_L = cReflect(Zl,Zo)
phd_GAMMA_L = cmplx2phd(GAMMA_L)

% Reflection coef. at the generator. 
GAMMA_G = cReflect(Rs,Zo)
phd_GAMMA_G = cmplx2phd(GAMMA_G)

% For point a) we follow B*L = (2Pi/lambda)*(lambda/2) = Pi .
Bl_a =  pi ;

% For point b) we follow B*L = (2Pi/lambda)*(lambda/4) = Pi / 2 .
Bl_b =  pi / 2 ;

% l is periodic of lambda / 2 
Zin_a = Zl; 
Zin_aa = Zo * (1 + GAMMA_L*exp(-1i*2*Bl_a))/(1 - GAMMA_L*exp(-1i*2*Bl_a));
phd_Zin_a = cmplx2phd(Zin_a) % Inductor 17.1908  -35.7667


% l is periodic of lambda / 4 
Zin_b = Zo*Zo/Zl;
Zin_bb = Zo * (1 + GAMMA_L*exp(-1i*2*Bl_b))/(1 - GAMMA_L*exp(-1i*2*Bl_b));
phd_Zin_b = cmplx2phd(Zin_b) % Capacitor 145.4270 +35.7667

PW_a = PW_missmatch(Vs, Zin_a, Rs)
PW_b= PW_missmatch(Vs, Zin_b, Rs)



%% Ex 1 - 4 
%--------------------------------------------------------------------------
% a) PH Refflection coef
% b) SWR
% c) Zin 
% d) Distance to Max voltage from Zl
%--------------------------------------------------------------------------
close all
clc

ee = 4.3;
F = 2*Gi;
Lambda = 299.8*Me/ (F * sqrt(ee))
l = 0.03
l/Lambda + 0.285 - 0.5
w = F * 2 * pi;
C = 2.65*p;
Zl = 90 + (1/(1i*w*C))
phd_Zl = cmplx2phd(Zl)
Zo = 50;
GAMMA = cReflect(Zl,Zo);
phd_GAMMA = cmplx2ph(GAMMA)
swr = SWR(GAMMA)
Zn = Zl / Zo
Zin = Bl_Zin(Zo, Zl, (2*pi/Lambda), l)
phd_Zin = cmplx2ph(Zin)
Zin/Zo


%% Ex 1 - 5 
%--------------------------------------------------------------------------
% a) Zt
% b) lx
% c) Zin_lx
% d) Distance to Max voltage from Zl
%--------------------------------------------------------------------------
close all
clc

Zo = 50;
Zl = 25 -1i*110
Zn = Zl / Zo
Zin = 4.25
Zt  = 50^2/Zin

vp = 0.15 * Gi;
F = 3*Gi;
Lambda = vp/ F
w = F * 2 * pi;


