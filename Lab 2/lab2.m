%%
clc
format longg
clear all
close all
mm = 1;

Er = 2.2;
H = 0.794 *mm; 
step = mm /1000;
W0 = 1.0333*mm;

ee0 = (Er+1)/2 + ((Er - 1)/2).* (1./(sqrt(1 +10.*(H./W0))));

F = 6e9;
c = 0.3e9;
Vp = c/sqrt(ee0);

lambda = Vp/F; 
L_4 = (lambda/4); % m
L_8 = L_4 / 2% m

Zo = (120*pi./sqrt(ee0)).* (1./((W0*1/H) + 1.393+0.667*log((W0*1/H)+1.444) ))
ZL = (60/sqrt(ee0))* log(8*(H/W0) + 0.25*(W0/H))





%%
%% Inciso C
clear; clc;

Z0_line = 50;
Zin = Z0_line;
ZL = 100;
syms Z0_transf
%Zin = Z0_transf*((ZL+1i*Z0_transf*tan(pi/2))/(Z0_transf+1i*ZL*tan(pi/2)));
eqn = Z0_transf*((ZL+1i*Z0_transf*tan(pi/2))/(Z0_transf+1i*ZL*tan(pi/2)))*1/Zin -1;
Z0_par = abs(double(vpasolve(eqn,Z0_transf)));
Z0 = Z0_par(1)

Er = 3.9;
H  = 60 ;
syms W;
eqn = (60/sqrt((Er+1)/2 + (Er-1)/(2*sqrt(1+(1*H)/W))))*log((8*H)/W + W/(4*H))*(1/Z0) - 1;
W1 = double(vpasolve(eqn,W,H));
eqn = ((120*pi)/sqrt((Er+1)/2 + (Er-1)/(2*sqrt(1+(10*H)/W)))) * 1/((W/H)+1.393+(2/3)*log(W/H +1.444)) * 1/Z0 -1;
W2 = double(vpasolve(eqn,W));
clear W;
if(W1/H <= 1)
    W = W1
end
if(W2/H > 1)
    W = W2
end

f = 3E9;
Ee = (Er+1)/2 + (Er-1)/(2*sqrt(1+(10*H)/W));
vp = 299792458/sqrt(Ee);
lambda = vp/f;
L = lambda/4
L = vpa(L)*vpa(39370.078740157)

%%

%% Inciso C
clear; clc;
format longg
mm = 1;
Z0 = 81.52
Er = 2.2;
H  = 0.794 * mm;
syms W;
eqn = (60/sqrt((Er+1)/2 + (Er-1)/(2*sqrt(1+(10*H)/W))))*log((8*H)/W + W/(4*H))*(1/Z0) - 1;
W1 = double(vpasolve(eqn,W,H));
eqn = ((120*pi)/sqrt((Er+1)/2 + (Er-1)/(2*sqrt(1+(10*H)/W)))) * 1/((W/H)+1.393+(2/3)*log(W/H +1.444)) * 1/Z0 -1;
W2 = double(vpasolve(eqn,W));
clear W;
if(W1/H <= 1)
    W = W1*mm
end
if(W2/H > 1)
    W = W2*mm
end
f = 6E9;
Ee = (Er+1)/2 + (Er-1)/(2*sqrt(1+(10*H)/W));
vp = 299792458/sqrt(Ee);
lambda = vp/f;
L = (lambda/8) +(lambda)