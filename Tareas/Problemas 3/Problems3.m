%% problems 3 - Ex 05
clc

S = sym('S', 4)

S1_1 = phd2cmplx(0.15,80);
S1_2 = phd2cmplx(0.9,45);
S1_3 = phd2cmplx(0.2,-45);
S1_4 = phd2cmplx(0,0);

S2_1 = phd2cmplx(0.9,45);
S2_2 = phd2cmplx(0,0);
S2_3 = phd2cmplx(0,0);
S2_4 = phd2cmplx(0.35,60);

S3_1 = phd2cmplx(0.2,-45);
S3_2 = phd2cmplx(0,0);
S3_3 = phd2cmplx(0,0);
S3_4 = phd2cmplx(0.75,-35);

S4_1 = phd2cmplx(0,0);
S4_2 = phd2cmplx(0.35,60);
S4_3 = phd2cmplx(0.75,-35);
S4_4 = phd2cmplx(0,0);



SMAT = [S1_1 S1_2 S1_3 S1_4 ; 
        S2_1 S2_2 S2_3 S2_4 ;
        S3_1 S3_2 S3_3 S3_4 ;
        S4_1 S4_2 S4_3 S1_4 ]
    
 SMAT_t = (SMAT)'
 SMAT_c = conj(SMAT)
 SMAT_ct = SMAT_c'
 
 SMAT_t * SMAT_c
 SMAT * SMAT_ct
 I = eye(4);
 Z = (I + SMAT) * inv(I - SMAT)
 Y = inv(Z)
    
 S1_1*conj(S1_1) +  S2_1*conj(S2_1)+  S3_1*conj(S3_1)+  S4_1*conj(S4_1)


%% Ejercicio 6
clear variables
close all
clc

f = 1e9;
Vs = 1.5;
Rs = 25;
Rl = 75;
Zo = 50;
er = 4;
l = 2.5e-2;
c = 0.3e9;

Gl = 1/Rl;
Yo = 1/Zo;

beta = (2*pi*f*sqrt(er))/c;

M1 = [1 Rs ; 0 1];
M2 = [cos(beta*l) 1j*Zo*sin(beta*l) ; 1j*Yo*sin(beta*l) cos(beta*l)];
M3 = [1 0 ; Gl 1];

M_abcd = (M1^-1)*(M2^-1)*(M3^-1);

Zl = Rl;
Zin = (Zo*((Zl + 1j*Zo*tan(beta*l))/Zo + 1j*Zl*tan(beta*l))) + Rs;
I1 = Vs/Zin;

V2 = M_abcd(1,1)*Vs + M_abcd(2,1)*I1;
abs(V2)
angle(V2)*180/pi



