%% Global variables definition.
%--------------------------------------------------------------------------
global p n u m Ki Me Gi;

p = 1e-12;
n = 1e-9;
u = 1e-6;
m = 1e-3;
Ki = 1e3;
Me = 1e6;
Gi = 1e9;

%% Problems 2 - 1
%--------------------------------------------------------------------------
%   A lossy transmission line has the following per-unit length parameters:
%   L = 289.05 nH/m
%   C = 115.62 pF/m 
%   R = 6.3 ?/m  
%   G = 5 mS/m. 
%
%   For  simplicity,  it  is  assumed  that  these  four  parameters  are 
%   frequency-independent. 
%
%   a) Calculate the characteristic impedance of the line (Z0)at:
%   -   10 MHz 
%   -   100 MHz
%
%   b) Make 3 plots versus frequency from 10 MHz to 10 GHz
%   -   |Z0|
%   -   ? (attenuation constant) 
%   -   ? (propagation constant)
%--------------------------------------------------------------------------
close all
clc

R = 6.3;
L = 289.05 * n;
C = 115.62 * p;
G = 5 * m;

% a1) Zo at 10 MHZ
w = (10*Me) * (2*pi);
Zo_10M = Zch(R,L,G,C,w)
Phd_Zo_10M = cmplx2phd(Zo_10M)
Phd_Zo_10M(1)
Phd_Zo_10M(2)

cmplx_Zo_10 = phd2cmplx(Phd_Zo_10M(1),Phd_Zo_10M(2))

% a2) Zo at 10 MHZ
w = (100*Me) * (2*pi);
Zo_100M = Zch(R,L,G,C,w)
Phd_Zo_100M = cmplx2phd(Zo_100M)

complex_gamma_plot(1*Me,10*Gi,R,L,G,C);
Zo_plot(1*Me,100*Gi,R,L,G,C);

%% Problems 2 - 2
%--------------------------------------------------------------------------
%   A lossy transmission line has the following per-unit length parameters:
%   L = 289.05 nH/m
%   C = 115.62 pF/m 
%   R = 6.3 ?/m  
%   G = 5 mS/m. 
%
%   The load impedance ZL consists of an 80 ? resistor in series with a 
%   10nH inductor. 
%
%   Calculate the input impedance of the transmission line Zin at 100 MHz,
%   at the following distances from the load:
%   -   a) l = 1 cm, 
%   -   b) l = 10 cm, 
%   -   c) l = 100 cm.
%--------------------------------------------------------------------------
close all
clc

R = 6.3;
L = 289.05 * n;
C = 115.62 * p;
G = 5 * m;

f = 100*Me; 
w = f*2*pi;
Zl = 80 + 10*n*w*1i;

g = cmplxPropag(R,L,G,C,w);
betha = imag(g);
lambda = 2*pi / betha ;
lambda_2 = lambda / 2 ;
lambda_4 = lambda / 4 ;

Zo = Zch(R,L,G,C,w);

Zin_1cm = Gl_Zin(Zo, Zl, g, 0.01);
Phd_Zin_1cm = cmplx2phd(Zin_1cm)

Zin_10cm = Gl_Zin(Zo, Zl, g, 0.1);
Phd_Zin_10cm = cmplx2phd(Zin_10cm)

Zin_1m = Gl_Zin(Zo, Zl, g, 1);
Phd_Zin_1m = cmplx2phd(Zin_1m)

%% Problems 2 - 3
%--------------------------------------------------------------------------
%   A lossless transmission line has the following per-unit length parameters:
%   L = 289.05 nH/m
%   C = 115.62 pF/m 
%   R = 0 ?/m  
%   G = 0 S/m. 
%
%   The load impedance ZL consists of an 80 ? resistor in series with a 
%   10nH inductor. 
%
%   Calculate the input impedance of the transmission line Zin at 100 MHz,
%   at the following distances from the load:
%   -   a) l = 1 cm, 
%   -   b) l = 10 cm, 
%   -   c) l = 100 cm.
%--------------------------------------------------------------------------
close all
clc

R = 0;
L = 289.05 * n;
C = 115.62 * p;
G = 0 ;

f = 100*Me; 
w = f*2*pi;

Zl = 80 + 10*n*w*1i;
Zo = Zch(R,L,G,C,w);

g = cmplxPropag(R,L,G,C,w);
betha = imag (g);
lambda = 2*pi / betha;

Zin_1cm = Bl_Zin(Zo, Zl, betha, 0.01);
Phd_Zin_1cm = cmplx2phd(Zin_1cm)

Zin_10cm = Bl_Zin(Zo, Zl, betha, 0.1);
Phd_Zin_10cm = cmplx2phd(Zin_10cm)

Zin_1m = Bl_Zin(Zo, Zl, betha, 1);
Phd_Zin_1m = cmplx2phd(Zin_1m)

%% Problems 2 - 7
%--------------------------------------------------------------------------
%   A lossless transmission line has
%   Vrms = 15 V
%   Zo = Rs = 75 Ohm/m (matched)
%   Zl = 5 - j30
%   L = ? H/m
%   C = ? F/m 
%   R = 0 Ohm/m  
%   G = 0 S/m. 
%
%   Calculate the average power delivered to the load if
%   -   a) l = lambda /4 
%   -   b) l = 0.3 * lambda 
%--------------------------------------------------------------------------
close all
clc

R = 0;
G = 0 ;
Rs = 75;
Zo = 75;
Zl = 55 - 30*1i;
VRMS = 15;  

% GAMMA is the reflection coef. 
GAMMA = cReflect(Zl,Zo);

% For point a) we follow B*L = (2Pi/lambda)*(lambda/4) = Pi/2 
Bl_a =  pi / 2;

% For point b) we follow B*L = (2Pi/lambda)*(0.3 * lambda) = 2Pi * 0.3
Bl_b =  2*pi * 0.3;

% When l is periodic of lambda / 4 we have max voltage and highest Zin.
Zin_a = Zo*Zo/Zl; 

%peak voltage at 15 sqrt(2) 
Vs =  VRMS * sqrt(2);

% Voltage divider
Vin = Vs * Zin_a / (Rs + Zin_a);

%As a linear combination of B*L, we use Bl_a * 1.
Vo_plus = Vo_plusL(Vin,Bl_a,1,GAMMA);
PW_avg_a  = PWavg(Vo_plus, GAMMA, Rs)

Zin_b = Bl_Zin(Zo,Zl,Bl_b,1); 

% Voltage divider
Vin = Vs * Zin_b / (Rs + Zin_b);

%As a linear combination of B*L, we use Bl_b * 1. 
Vo_plus = Vo_plusL(Vin,Bl_b,1,GAMMA);
PW_avg_b  = PWavg(Vo_plus, GAMMA, Rs)

%% Problems 2 - 7b
%--------------------------------------------------------------------------
%   A lossless transmission line has
%   Vrms = 15 V
%   Zo = 50 Ohm/m 
%   Rs = 75 Ohm/m (missmatched)
%   Zl = 5 - j30 Ohm/m 
%   L = ? H/m
%   C = ? F/m 
%   R = 0 Ohm/m  
%   G = 0 S/m. 
%
%   Calculate the average power delivered to the load if
%   -   a) l = lambda /4 
%   -   b) l = 0.3 * lambda 
%--------------------------------------------------------------------------
close all
clc

R = 0;
G = 0 ;
Rs = 75;
Zo = 75;
Zl = 55 - 30*1i;
VRMS = 15;  

% GAMMA_L is the reflection coef at the load. 
GAMMA_L = cReflect(Zl,Zo)

% GAMMA_L is the reflection coef at the generator. 
GAMMA_G = cReflect(Rs,Zo);

% For point a) we follow B*L = (2Pi/lambda)*(lambda/4) = Pi/2 
Bl_a =  pi / 2;

% For point b) we follow B*L = (2Pi/lambda)*(0.3 * lambda) = 2Pi * 0.3
Bl_b =  2*pi * 0.3;

% When l is periodic of lambda / 4 we have max voltage and highest Zin.
Zin_a = Zo*Zo/Zl;

%peak voltage at 15 sqrt(2) 
Vs =  VRMS * sqrt(2);

% Voltage divider
Vin = Vs * Zo / (Rs + Zo);

%As a linear combination of B*L, we use Bl_a * 1.
Vo_plus = Vo_plusReflect(Vin,Bl_a,1,GAMMA_L,GAMMA_G );
PW_avg_a  = PWavg(Vo_plus, GAMMA, Rs)
PW_missmatch(Vs, Zin_a, Rs)

Zin_b = Bl_Zin(Zo,Zl,Bl_b,1); 

% Voltage divider
Vin = Vs * Zo / (Rs + Zo);

%As a linear combination of B*L, we use Bl_b * 1. 
Vo_plus = Vo_plusReflect(Vin,Bl_a,1,GAMMA_L,GAMMA_G );
PW_avg_b  = PWavg(Vo_plus, GAMMA, Rs)
PW_missmatch(Vs, Zin_b, Rs)

%% Functions Section
%--------------------------------------------------------------------------
function complex_gamma_plot(w_min, w_max,R, L, G, C)
w0 = w_min:1e6:w_max;
k = numel(w0);
alpha = zeros(k,1);
size(alpha);
betha = zeros(k,1);

for j = 1:k
    w = w0(1 , j)  * (2*pi);
    g = cmplxPropag(R, L, G, C, w);
    alpha(j, 1) = real(g);     
    betha(j, 1) = imag(g);
    
end

figure;
plot (w0, alpha)
title("Attenuation constant")
xlim([w_min w_max])
ylim([0 2*max(alpha)]) 

figure;
plot (w0, betha)
title("Propagation constant")
xlim([w_min w_max])
end


function Zo_plot(w_min, w_max,R, L, G, C)
w0 = w_min:1e6:w_max;
k = numel(w0);
Zetha_abs = zeros(k,1);

for j = 1:k
    w = w0(1 , j)  * (2*pi);
    Zo = Zch(R,L,G,C,w);
    Zo_abs(j, 1) = abs(Zo);   
    
end

figure;
plot (w0, Zo_abs)
title("Characteristic Impedance")
xlim([w_min w_max])
ylim([0 2*max(Zo_abs)]) 
end
%--------------------------------------------------------------------------