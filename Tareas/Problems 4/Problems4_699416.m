%% Global parameters
clc;
clear all;
close all;
format longg
global c p n u m Ki Me Gi;
c = 299792458;                      % meter / second
p = 1e-12;                          % pico
n = 1e-9;                           % nano
u = 1e-6;                           % micro
m = 1e-3;                           % mili
Ki = 1e3;                           % Kilo
Me = 1e6;                           % Mega
Gi = 1e9;                           % Giga

%% Problems 4 - Ex 1
%   Plot the Bouncing diagrams with the following parameters: 
%   -   Ind = 333.325*n             [H / meter]
%   -   Cap = 133.33*p              [F / meter] 
%   -   Len = 1                     [meter]
%   -   Vs = 3                      [volt]
%   -   Time = 80*n                 [second]
clc;
close all;

length = 1;                         % TL lenght.
R = 0;                              % Lossless, resistance.
G = 0 ;                             % Lossless, transconductance.
L = (333.325*n)*length;             % Lossless, inductance.
C = (133.33*p)*length;              % Lossless, capacitance.
w = 1e-12;                          % Frequency independent 
Zo =  Z0(R, L, G, C, w)             % 50 ohm characteristic impedance
vp = 1 / sqrt(L*C);                 % Lossless, phase velocity.
td = length/vp;                     % Time delay in the TL. 
tmax = 5*n;                       % Simulation time.
samples = 1000;                     % Samples in simulation;
ts = tmax/samples;                  % Time per sample, plot resolution.   


% d) Rs = 5 and RL  = 10k
Vs = 3;                             % Source voltage [volt].
Rs = 5;                             % Source resistance [ohm].
Rl  = 10*Ki;                        % Load resistance [ohm].
Gamma_s = cReflect(Rs,Zo);          % Source reflection coefficient.
Trans_s = 1+ Gamma_s;               % Source transmission coefficient. 
Gamma_l = cReflect(Rl,Zo);          % Load reflection coefficient.
Trans_l = 1+ Gamma_l;               % Load transmission coefficient. 
V_initial = Vs*Zo/(Zo + Rs);         % Initial voltage at the entrance.

nbounce = floor(tmax/td);           % Number of bounces to plot.

bounce_vec_V = zeros(nbounce,1);    % Bounce voltages vector data. 
bounce_vec_I = zeros(nbounce,1);    % Bounce currents vector data. 

acc_vec_Vl = zeros(nbounce,1);      % Voltage at load vector data. 
acc_vec_Vs = zeros(nbounce,1);      % Voltage at source vector data. 
acc_vec_Il = zeros(nbounce,1);      % Current at load vector data. 
acc_vec_Is = zeros(nbounce,1);      % Current at source vector data. 

bounce_vec_V(1,1) = V_initial;
bounce_vec_I(1,1) = V_initial/Zo;

acc_vec_Vs(1,1) = V_initial;          % Voltage at source initial value. 
acc_vec_Vl(1,1) = 0;                 % Voltage at load initial value. 
acc_vec_Is(1,1) = V_initial/Zo;       % Voltage at source initial value. 
acc_vec_Il(1,1) = 0;                 % Voltage at load initial value.

for itt = 2 : nbounce
    if mod(itt,2) ~= 0 % odd, multiply by Gamma_s
        
        bounce_vec_V(itt,1) = bounce_vec_V(itt-1,1)*Gamma_s;        
        bounce_vec_I(itt,1) = bounce_vec_I(itt-1,1)*Gamma_s*-1;
        
        acc_vec_Vs(itt,1) = acc_vec_Vs(itt-1,1)+ bounce_vec_V(itt,1)+bounce_vec_V(itt-1,1);
        acc_vec_Vl(itt,1) = acc_vec_Vl(itt-1,1);   
        
        acc_vec_Is(itt,1) = acc_vec_Is(itt-1,1)+bounce_vec_I(itt,1)+bounce_vec_I(itt-1,1); 
        acc_vec_Il(itt,1) = acc_vec_Il(itt-1,1);   
        
        counter_ts = counter_ts + 1;  
        
    else % even multiply by -Gamma_l
        
        bounce_vec_V(itt,1) = bounce_vec_V(itt-1,1)*Gamma_l;            
        bounce_vec_I(itt,1) = bounce_vec_I(itt-1,1)*Gamma_l*-1;
        
        acc_vec_Vl(itt,1) = acc_vec_Vl(itt-1,1)+bounce_vec_V(itt,1)+bounce_vec_V(itt-1,1);
        acc_vec_Vs(itt,1) = acc_vec_Vs(itt-1,1);     
        
        acc_vec_Il(itt,1) = acc_vec_Il(itt-1,1)+bounce_vec_I(itt,1)+bounce_vec_I(itt-1,1);
        acc_vec_Is(itt,1) = acc_vec_Is(itt-1,1);  
        
        counter_tl = counter_tl + 1;
    end          

end

% Pulse Shape: 
mp = ceil(samples/nbounce);
base_pulse = ones(1,mp);

% Impulse train:
s_Vs = zeros(1,samples);
s_Vl = zeros(1,samples);
s_Vs(1 :mp:end ) = acc_vec_Vs;
s_Vl(1 :mp:end ) = acc_vec_Vl;

s_Is = zeros(1,samples);
s_Il = zeros(1,samples);
s_Is(1 :mp:end ) = acc_vec_Is;
s_Il(1 :mp:end ) = acc_vec_Il;

% Pulse train.

signal_Vs = conv(base_pulse,s_Vs);
signal_Vl = conv(base_pulse,s_Vl);

signal_Is = conv(base_pulse,s_Is);
signal_Il = conv(base_pulse,s_Il);

% Display : Boncing voltages
t = ts:ts:tmax;                     % Time vector;
T_ones = ones(1,samples);           % Equivalent time signal.  
Vs_time = conv(Vs,T_ones);          % Time-domain voltage signal.

figure;


% Voltages plot
subplot(2,1,1 );
hold on
plot(t,signal_Vl(1:samples),'g')
plot(t,signal_Vs(1:samples),'b')
plot(t,Vs_time(1:samples),'r')
hold off
grid on;
legend('Voltage at the load','Voltage at the entrance','Voltage at the source')
ylabel("Voltage [V]")
xlabel("Time [ns]")
title("Voltages plot: Time delay = 1 ns, unmatched Rs = 5 and RL  = 10k ")


% Currents plot
subplot(2,1,2);
hold on
plot(t,signal_Il(1:samples),'g')
plot(t,signal_Is(1:samples),'b')
hold off
grid on;
legend('Current at the load','Current at the entrance')
ylabel("Current [A]")
xlabel("Time [ns]")

title("Currents plot: Time delay = 1 ns, unmatched Rs = 5 and RL  = 10k ")




%% Problems 4 - Ex 2 
%   Plot the Voltage Bouncing diagrams with the following parameters: 
%   -   Len = 0.15                  [meter]
%   -   Vs = 2                      [volt]
%   -   Rs = 20                     [ohm]
%   -   Rl = 200                    [ohm]
%   -   Zo = 50                     [ohm]
%   -   ee = 4                      [ ]
%   -   Time = 10*n                 [second]
clc;
close all;

length = 0.15;                      % TL lenght.
Zo =  50;                           % 50 ohm characteristic impedance
ee = 4;                             % Effective permitivity.
vp = 0.3e9 / sqrt(ee);              % Lossless, phase velocity.
td = length/vp;                     % Time delay in the TL. 
tmax = 10*n;                        % Simulation time.
samples = 1000;                     % Samples in simulation;
ts = tmax/samples;                  % Time per sample, plot resolution. 


% d) Rs = 5 and RL  = 10k
Vs = 2;                             % Source voltage [volt].
Rs = 20;                            % Source resistance [ohm].
Rl  = 200;                          % Load resistance [ohm].
Gamma_s = cReflect(Rs,Zo);          % Source reflection coefficient.
Trans_s = 1+ Gamma_s;               % Source transmission coefficient. 
Gamma_l = cReflect(Rl,Zo);          % Load reflection coefficient.
Trans_l = 1+ Gamma_l;               % Load transmission coefficient. 
V_initial = Vs*Zo/(Zo + Rs);        % Initial voltage at the entrance.

nbounce = floor(tmax/td);           % Number of bounces to plot.

bounce_vec_V = zeros(nbounce,1);    % Bounce voltages vector data. 
bounce_vec_I = zeros(nbounce,1);    % Bounce currents vector data. 

acc_vec_Vl = zeros(nbounce,1);      % Voltage at load vector data. 
acc_vec_Vs = zeros(nbounce,1);      % Voltage at source vector data. 
acc_vec_Il = zeros(nbounce,1);      % Current at load vector data. 
acc_vec_Is = zeros(nbounce,1);      % Current at source vector data. 

bounce_vec_V(1,1) = V_initial;
bounce_vec_I(1,1) = V_initial/Zo;

acc_vec_Vs(1,1) = V_initial;          % Voltage at source initial value. 
acc_vec_Vl(1,1) = 0;                 % Voltage at load initial value. 
acc_vec_Is(1,1) = V_initial/Zo;       % Voltage at source initial value. 
acc_vec_Il(1,1) = 0;                 % Voltage at load initial value.

for itt = 2 : nbounce
    if mod(itt,2) ~= 0 % odd, multiply by Gamma_s
        
        bounce_vec_V(itt,1) = bounce_vec_V(itt-1,1)*Gamma_s;        
        bounce_vec_I(itt,1) = bounce_vec_I(itt-1,1)*Gamma_s*-1;
        
        acc_vec_Vs(itt,1) = acc_vec_Vs(itt-1,1)+ bounce_vec_V(itt,1)+bounce_vec_V(itt-1,1);
        acc_vec_Vl(itt,1) = acc_vec_Vl(itt-1,1);   
        
        acc_vec_Is(itt,1) = acc_vec_Is(itt-1,1)+bounce_vec_I(itt,1)+bounce_vec_I(itt-1,1); 
        acc_vec_Il(itt,1) = acc_vec_Il(itt-1,1);   
        
        counter_ts = counter_ts + 1;  
        
    else % even multiply by -Gamma_l
        
        bounce_vec_V(itt,1) = bounce_vec_V(itt-1,1)*Gamma_l;            
        bounce_vec_I(itt,1) = bounce_vec_I(itt-1,1)*Gamma_l*-1;
        
        acc_vec_Vl(itt,1) = acc_vec_Vl(itt-1,1)+bounce_vec_V(itt,1)+bounce_vec_V(itt-1,1);
        acc_vec_Vs(itt,1) = acc_vec_Vs(itt-1,1);     
        
        acc_vec_Il(itt,1) = acc_vec_Il(itt-1,1)+bounce_vec_I(itt,1)+bounce_vec_I(itt-1,1);
        acc_vec_Is(itt,1) = acc_vec_Is(itt-1,1);  
        
        counter_tl = counter_tl + 1;
    end          

end

% Pulse Shape: 
mp = ceil(samples/nbounce);
base_pulse = ones(1,mp);

% Impulse train:
s_Vs = zeros(1,samples);
s_Vl = zeros(1,samples);
s_Vs(1 :mp:end ) = acc_vec_Vs;
s_Vl(1 :mp:end ) = acc_vec_Vl;

s_Is = zeros(1,samples);
s_Il = zeros(1,samples);
s_Is(1 :mp:end ) = acc_vec_Is;
s_Il(1 :mp:end ) = acc_vec_Il;

% Pulse train.

signal_Vs = conv(base_pulse,s_Vs);
signal_Vl = conv(base_pulse,s_Vl);

signal_Is = conv(base_pulse,s_Is);
signal_Il = conv(base_pulse,s_Il);

% Display : Boncing voltages
t = ts:ts:tmax;                     % Time vector;
T_ones = ones(1,samples);           % Equivalent time signal.  
Vs_time = conv(Vs,T_ones);          % Time-domain voltage signal.

figure;


% Voltages plot
subplot(2,1,1 );
hold on
plot(t,signal_Vl(1:samples),'g')
plot(t,signal_Vs(1:samples),'b')
plot(t,Vs_time(1:samples),'r')
hold off
grid on;
legend('Voltage at the load','Voltage at the entrance','Voltage at the source')
ylabel("Voltage [V]")
xlabel("Time [ns]")
title("Voltages plot: Time delay = 1 ns, unmatched Rs = 20 and RL  = 200 ")


% Currents plot
subplot(2,1,2);
hold on
plot(t,signal_Il(1:samples),'g')
plot(t,signal_Is(1:samples),'b')
hold off
grid on;
legend('Current at the load','Current at the entrance')
ylabel("Current [A]")
xlabel("Time [ns]")

title("Currents plot: Time delay = 1 ns, unmatched Rs = 20 and RL  = 200 ")


%% Problems 4 - Ex 2 - A
%   Plot the Voltage Bouncing diagrams with the following parameters: 
%   -   Len = 0.1                   [meter]
%   -   Vpulse = 5                  [volt]
%   -   Pwidth = 2.5*n              [second] 
%   -   Rs = 15                     [ohm]
%   -   Rl = 150                    [ohm]
%   -   Zo = 50                     [ohm]
%   -   ee = 9                      [ ]
%   -   Time = 8*n                 [second]
clc;
close all;

length = 0.10;                      % TL lenght.
Zo =  50;                           % 50 ohm characteristic impedance
ee = 9;                             % Effective permitivity.
vp = 0.3e9 / sqrt(ee);              % Lossless, phase velocity.
td = length/vp;                     % Time delay in the TL. 
tmax = 40*n;                         % Simulation time.
samples = 1000;                     % Samples in simulation;
ts = tmax/samples;                  % Time per sample, plot resolution. 


% d) Rs = 5 and RL  = 10k
Vs = 5;                             % Vpulse source voltage [volt].
pw = 2.5*n;                         % Pulse width
Rs = 15;                            % Source resistance [ohm].
Rl  = 150;                          % Load resistance [ohm].
Gamma_s = cReflect(Rs,Zo);          % Source reflection coefficient.
Trans_s = 1+ Gamma_s;               % Source transmission coefficient. 
Gamma_l = cReflect(Rl,Zo);          % Load reflection coefficient.
Trans_l = 1+ Gamma_l;               % Load transmission coefficient. 
V_initial = Vs*Zo/(Zo + Rs);        % Initial voltage at the entrance.

nbounce = floor(tmax/td);           % Number of bounces to plot.

bounce_vec_V = zeros(nbounce,1);    % Bounce voltages vector data. 
bounce_vec_I = zeros(nbounce,1);    % Bounce currents vector data. 

acc_vec_Vl = zeros(nbounce,1);      % Voltage at load vector data. 
acc_vec_Vs = zeros(nbounce,1);      % Voltage at source vector data. 
acc_vec_Il = zeros(nbounce,1);      % Current at load vector data. 
acc_vec_Is = zeros(nbounce,1);      % Current at source vector data. 

bounce_vec_V(1,1) = V_initial;
bounce_vec_I(1,1) = V_initial/Zo;

acc_vec_Vs(1,1) = V_initial;          % Voltage at source initial value. 
acc_vec_Vl(1,1) = 0;                 % Voltage at load initial value. 
acc_vec_Is(1,1) = V_initial/Zo;       % Voltage at source initial value. 
acc_vec_Il(1,1) = 0;                 % Voltage at load initial value.

for itt = 2 : nbounce
    if mod(itt,2) ~= 0 % odd, multiply by Gamma_s
        
        bounce_vec_V(itt,1) = bounce_vec_V(itt-1,1)*Gamma_s;        
        bounce_vec_I(itt,1) = bounce_vec_I(itt-1,1)*Gamma_s*-1;
        
        acc_vec_Vs(itt,1) = acc_vec_Vs(itt-1,1)+ bounce_vec_V(itt,1)+bounce_vec_V(itt-1,1);
        acc_vec_Vl(itt,1) = acc_vec_Vl(itt-1,1);   
        
        acc_vec_Is(itt,1) = acc_vec_Is(itt-1,1)+bounce_vec_I(itt,1)+bounce_vec_I(itt-1,1); 
        acc_vec_Il(itt,1) = acc_vec_Il(itt-1,1);   
        
        counter_ts = counter_ts + 1;  
        
    else % even multiply by -Gamma_l
        
        bounce_vec_V(itt,1) = bounce_vec_V(itt-1,1)*Gamma_l;            
        bounce_vec_I(itt,1) = bounce_vec_I(itt-1,1)*Gamma_l*-1;
        
        acc_vec_Vl(itt,1) = acc_vec_Vl(itt-1,1)+bounce_vec_V(itt,1)+bounce_vec_V(itt-1,1);
        acc_vec_Vs(itt,1) = acc_vec_Vs(itt-1,1);     
        
        acc_vec_Il(itt,1) = acc_vec_Il(itt-1,1)+bounce_vec_I(itt,1)+bounce_vec_I(itt-1,1);
        acc_vec_Is(itt,1) = acc_vec_Is(itt-1,1);  
        
        counter_tl = counter_tl + 1;
    end          

end

% Pulse Shape: 
mp = ceil(samples/nbounce);
base_pulse = ones(1,mp);

% Impulse train:
s_Vs = zeros(1,samples);
s_Vl = zeros(1,samples);
s_Vs(1 :mp:end ) = acc_vec_Vs;
s_Vl(1 :mp:end ) = acc_vec_Vl;

s_Is = zeros(1,samples);
s_Il = zeros(1,samples);
s_Is(1 :mp:end ) = acc_vec_Is;
s_Il(1 :mp:end ) = acc_vec_Il;

% Pulse train.

signal_Vs = conv(base_pulse,s_Vs);
signal_Vl = conv(base_pulse,s_Vl);

signal_Is = conv(base_pulse,s_Is);
signal_Il = conv(base_pulse,s_Il);

% Display : Boncing voltages
t = ts:ts:tmax;                     % Time vector;
T_ones = ones(1,samples);           % Equivalent time signal.  
Vs_time = conv(Vs,T_ones);          % Time-domain voltage signal.

figure;


% Voltages plot
subplot(2,1,1 );
hold on
plot(t,signal_Vl(1:samples),'g')
plot(t,signal_Vs(1:samples),'b')
plot(t,Vs_time(1:samples),'r')
hold off
grid on;
legend('Voltage at the load','Voltage at the entrance','Voltage at the source')
ylabel("Voltage [V]")
xlabel("Time [ns]")
title("Voltages plot: Time delay = 1 ns, unmatched Rs = 20 and RL  = 200 ")


% Currents plot
subplot(2,1,2);
hold on
plot(t,signal_Il(1:samples),'g')
plot(t,signal_Is(1:samples),'b')
hold off
grid on;
legend('Current at the load','Current at the entrance')
ylabel("Current [A]")
xlabel("Time [ns]")

title("Currents plot: Time delay = 1 ns, unmatched Rs = 20 and RL  = 200 ")

%% Problems 4 - Examen 3 - ex 2
%   Plot the Voltage Bouncing diagrams with the following parameters: 
%   -   Len = 0.1                   [meter]
%   -   Vpulse = 5                  [volt]
%   -   Pwidth = 2.5*n              [second] 
%   -   Rs = 20                     [ohm]
%   -   Rl = 180                    [ohm]
%   -   Zo = 50                     [ohm]
%   -   ee = 9                      [ ]
%   -   Time = 8*n                 [second]
clc;
close all;

length = 0.10;                      % TL lenght.
Zo =  50;                           % 50 ohm characteristic impedance
ee = 9;                             % Effective permitivity.
vp = 0.3e9 / sqrt(ee);              % Lossless, phase velocity.
td = length/vp;                     % Time delay in the TL. 
tmax = 5*n;                         % Simulation time.
samples = 1000;                     % Samples in simulation;
ts = tmax/samples;                  % Time per sample, plot resolution. 

% d) Rs = 5 and RL  = 10k
Vs = 3.3;                             % Vpulse source voltage [volt].
Rs = 20;                            % Source resistance [ohm].
Rl  = 180;                          % Load resistance [ohm].
Gamma_s = cReflect(Rs,Zo);          % Source reflection coefficient.
Trans_s = 1+ Gamma_s;               % Source transmission coefficient. 
Gamma_l = cReflect(Rl,Zo);          % Load reflection coefficient.
Trans_l = 1+ Gamma_l;               % Load transmission coefficient. 
V_initial = Vs*Zo/(Zo + Rs);        % Initial voltage at the entrance.

nbounce = floor(tmax/td);           % Number of bounces to plot.

bounce_vec_V = zeros(nbounce,1);    % Bounce voltages vector data. 
bounce_vec_I = zeros(nbounce,1);    % Bounce currents vector data. 

acc_vec_Vl = zeros(nbounce,1);      % Voltage at load vector data. 
acc_vec_Vs = zeros(nbounce,1);      % Voltage at source vector data. 
acc_vec_Il = zeros(nbounce,1);      % Current at load vector data. 
acc_vec_Is = zeros(nbounce,1);      % Current at source vector data. 

bounce_vec_V(1,1) = V_initial;
bounce_vec_I(1,1) = V_initial/Zo;

acc_vec_Vs(1,1) = V_initial;          % Voltage at source initial value. 
acc_vec_Vl(1,1) = 0;                 % Voltage at load initial value. 
acc_vec_Is(1,1) = V_initial/Zo;       % Voltage at source initial value. 
acc_vec_Il(1,1) = 0;                 % Voltage at load initial value.

for itt = 2 : nbounce
    if mod(itt,2) ~= 0 % odd, multiply by Gamma_s
        
        bounce_vec_V(itt,1) = bounce_vec_V(itt-1,1)*Gamma_s;        
        bounce_vec_I(itt,1) = bounce_vec_I(itt-1,1)*Gamma_s*-1;
        
        acc_vec_Vs(itt,1) = acc_vec_Vs(itt-1,1)+ bounce_vec_V(itt,1)+bounce_vec_V(itt-1,1);
        acc_vec_Vl(itt,1) = acc_vec_Vl(itt-1,1);   
        
        acc_vec_Is(itt,1) = acc_vec_Is(itt-1,1)+bounce_vec_I(itt,1)+bounce_vec_I(itt-1,1); 
        acc_vec_Il(itt,1) = acc_vec_Il(itt-1,1);    
        
    else % even multiply by -Gamma_l
        
        bounce_vec_V(itt,1) = bounce_vec_V(itt-1,1)*Gamma_l;            
        bounce_vec_I(itt,1) = bounce_vec_I(itt-1,1)*Gamma_l*-1;
        
        acc_vec_Vl(itt,1) = acc_vec_Vl(itt-1,1)+bounce_vec_V(itt,1)+bounce_vec_V(itt-1,1);
        acc_vec_Vs(itt,1) = acc_vec_Vs(itt-1,1);     
        
        acc_vec_Il(itt,1) = acc_vec_Il(itt-1,1)+bounce_vec_I(itt,1)+bounce_vec_I(itt-1,1);
        acc_vec_Is(itt,1) = acc_vec_Is(itt-1,1);  

    end          

end

% Pulse Shape: 
mp = ceil(samples/nbounce);
base_pulse = ones(1,mp);

% Impulse train:
s_Vs = zeros(1,samples);
s_Vl = zeros(1,samples);
s_Vs(1 :mp:end ) = acc_vec_Vs;
s_Vl(1 :mp:end ) = acc_vec_Vl;

s_Is = zeros(1,samples);
s_Il = zeros(1,samples);
s_Is(1 :mp:end ) = acc_vec_Is;
s_Il(1 :mp:end ) = acc_vec_Il;

% Pulse train.

signal_Vs = conv(base_pulse,s_Vs);
signal_Vl = conv(base_pulse,s_Vl);

signal_Is = conv(base_pulse,s_Is);
signal_Il = conv(base_pulse,s_Il);

% Display : Boncing voltages
t = ts:ts:tmax;                     % Time vector;
T_ones = ones(1,samples);           % Equivalent time signal.  
Vs_time = conv(Vs,T_ones);          % Time-domain voltage signal.

figure;


% Voltages plot
subplot(1,1,1 );

plot(t,Vs_time)

grid on;
ylim([-1 3.3])
ylabel("Voltage [V]")
xlabel("Time delay [td]")

title("Voltages plot: Time delay = 1 ns, unmatched Rs = 20 and RL  = 200 ")



